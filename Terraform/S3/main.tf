provider "aws" {
  profile = "default"
  region  = "eu-central-1"
}

resource "aws_s3_bucket" "b" {
  bucket = "getir-assignment"
  acl    = "private"

  tags = {
    Name        = "test bucket"
  }
}